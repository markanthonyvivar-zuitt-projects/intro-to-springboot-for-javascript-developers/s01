package com.zuitt.hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class HelloApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloApplication.class, args);
	}


//	@GetMapping("api/hello")
//	public String hello() {
//		return String.format("Hello World!");
//	}

//	@GetMapping("api/hello")
//	public String hello(@RequestParam(value="name", defaultValue = "unknown") String name){
//		return String.format("Hello, %s",name);
//	}
//
//	@GetMapping("api/friend")
//	public String friend(@RequestParam(value = "name") String name, @RequestParam(value = "age") String age){
//		return String.format("Hello, My name is %s, I am %s years old.", name, age);
//	}
//
//	@GetMapping("api/your-name/{name}")
//	public String yourName(@PathVariable("name") String name) {
//		return String.format("Hello, %s! How are you doing today?", name);
//	}


//	FIRST ACTIVITY FOR SPRINGBOOT

	@GetMapping("api/hi")
	public String username(@RequestParam(value="username") String username){
		return String.format("My username is %s.", username);
	}


	@GetMapping("api/course")
	public String course(@RequestParam(value="userName") String userName, @RequestParam(value="course") String course){
		return String.format("My username is %s and I take course %s", userName, course);
	}

}
